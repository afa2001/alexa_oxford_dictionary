var APP_ID = "amzn1.ask.skill.28cea699-b549-4b2a-bcc3-fd15c83913a2";
var APP_NAME = "Oxford_Dictinory";

var BASE_URL = "https://od-api.oxforddictionaries.com/api/v1/entries/en/";
var SUFFIX = "/definitions";
var API_APP_ID = "9ab7c518";
var API_KEY = "e0ab1c70c906063ef56e133423340a88";

var unirest = require('unirest')

/**
 * The AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('./AlexaSkill');

var LookupDictionary = function () {
    AlexaSkill.call(this, APP_ID);
};

// Extend AlexaSkill
LookupDictionary.prototype = Object.create(AlexaSkill.prototype);
LookupDictionary.prototype.constructor = LookupDictionary;

LookupDictionary.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    console.log("LookupDictionary onSessionStarted requestId: " + sessionStartedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // any initialization logic goes here
};

LookupDictionary.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    console.log("LookupDictionary onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    response.ask("Which word would you like me to look up the dictionary?", "Say an Englis word, like computer");
};

/**
 * Overridden to show that a subclass can override this function to teardown session state.
 */
LookupDictionary.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    console.log("LookupDictionary onSessionEnded requestId: " + sessionEndedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // any cleanup logic goes here
};

LookupDictionary.prototype.intentHandlers = {
    "WordSpelled": function (intent, session, response) {
        var spelledWord = intent.slots.Letter;
        
        console.log(spelledWord);
        console.log(spelledWord.value);
        
        /*    
        wordTrimmed = '';
        for(i = 0; i < spelledWord.value.length; i++)
        {
            if(spelledWord.value[i] != ' ')
            {
                wordTrimmed += spelledWord.value[i];
            }
        }
        */
        wordTrimmed = spelledWord.value.replace(/[^a-zA-Z]/g, '');
        console.log('To look up: ', wordTrimmed);  
        // send the data to API endpoint
        handleRequest(wordTrimmed, response);
        
    },

    "AMAZON.HelpIntent": function (intent, session, response) {
        response.ask("I can tell you the currency exchange between various currencies. What exchange rate do you want me to get for you?");
    },

    "AMAZON.StopIntent": function (intent, session, response) {
        response.tell("Bye. Thanks for using my services.");
    },

    "AMAZON.CancelIntent": function (intent, session, response) {
        response.tell("Bye. Thanks for using my services.");
    }
};

function handleRequest(word, response) {
    console.log('look up word: ', word)
    lookUpWord(word, function(err, body){
        if (err) {
            console.log(err)
            response.tell('Sorry, dictionary service is experiencing a problem with your request. Please try later');
        } else {
            var speech = "<speak>";
            speech += word + "<break time='3s'/>";
            for(i = 0; i < body.length - 1; i++)
            {
                speech += body[i] + "<break time='3s'/>";
            }
            speech += body[body.length - 1];

            speech += "</speak>";

            speechOutput = {
                speech: speech,
                type: AlexaSkill.speechOutputType.SSML
            };
            response.tell(speechOutput);   
        }
    });
    
}

function lookUpWord(word, callback)
{
    var response = [];
    unirest.get(BASE_URL+word+SUFFIX)
        .header("Accept", "application/json")
        .header("app_id", API_APP_ID)
        .header("app_key", API_KEY)
        .end(function (result) {
            console.log(result);
            if(result.code != '200'){
                callback(new Error('Error has occured'));
            } else {
                
                wordDefinition = result.body.results[0].lexicalEntries[0].entries[0].senses;
                for(i = 0; i < wordDefinition.length; i++)
                {
                    response.push(wordDefinition[i].definitions[0]);   
                }
                
                callback(null, response);
            }       
        });
}


// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    // Create an instance of the LookupDictionary skill.
    var skill = new LookupDictionary();
    skill.execute(event, context);
};

